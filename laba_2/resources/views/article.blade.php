<h3>{{$article->name}}</h3>
<i>{{$article->date}}</i>
@if($article->full_image_path)
    <img src="{{$article->full_image_path}}"/>
@endif
<p>{{$article->desc}}</p>



<h4>Comments</h4>
<ul>
@foreach($article->comments as $comment)
    <li>{{$comment->author}} <i>{{$comment->text}}</i></li>
@endforeach
</ul>

<div>
<form method="POST" action="/articles/{{$article->id}}/comments">
    @csrf
    <div>
        <input name="author"/>
    </div>
    <div>
    <textarea name="text"></textarea>
    </div>
    <input type="submit" value="Create">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
</form>

</div>
