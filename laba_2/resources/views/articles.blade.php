<ul>
    @foreach($articles as $article)
        <li>
            <a href="/articles/{{$article->id}}">
                {{$article->name}}
            </a>
            <i>{{$article->shortDesc}}</i>
        </li>
    @endforeach
</ul>
