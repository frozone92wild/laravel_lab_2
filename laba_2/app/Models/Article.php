<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $appends = ['full_image_path'];

    protected $fillable = [
        'name',
        'preview_image',
        'full_image',
        'shortDesc',
        'desc',
    ];

    public function getFullImagePathAttribute(){
        return '/images/'.$this->full_image;
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
