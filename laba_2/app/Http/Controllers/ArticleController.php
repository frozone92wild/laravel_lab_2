<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Comment;
use Illuminate\Http\Request;
use Nette\Schema\ValidationException;

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::all();

        return view('articles',['articles'=>$articles]);
    }

    public function get($articleId) {
        $article = Article::with('comments')->findOrFail($articleId);

        return view('article',['article'=> $article]);
    }

    public function storeComment($articleId, Request $request){
        try {
            $validated = $request->validate(
                [
                    'author' => 'required|min:4',
                    'text' => 'required'
                ]
            );

            $article = Article::findOrFail($articleId);

            $newComment = new Comment();
            $newComment->fill($validated);
            $newComment->article()->associate($article);
            $newComment->save();

            return redirect()->back();
        }catch (\Exception $exception){
            return redirect()->back()->withErrors($exception->validator);
        }
    }
}
