<?php

namespace Database\Seeders;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents(public_path().'/articles.json');
        $articles = json_decode($json,true);

        foreach ($articles as $article){


            $article['date'] = Carbon::parse($article['date'])->format('Y-m-d');


            $newArticle = new Article();
            $newArticle->fill($article);

            $newArticle->save();
        }
    }
}
